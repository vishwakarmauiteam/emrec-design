$(function () {
    $('#container').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: ['HR', 'IT', 'Finance', 'Sales', 'Marketing', 'Customer Experience', 'Facilities' ]
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
       /* legend: {
            align: 'right',
            x: 00,
            verticalAlign: 'top',
            y: 00,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
            borderColor: '#CCC',
            borderWidth: 1,
            shadow: false
        },*/
        tooltip: {
            headerFormat: '<b>{point.x}</b><br/>',
            pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
        },
        credits: {
      enabled: false
         },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true,
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                    style: {
                        textShadow: '0 0 3px black'
                    }
                }
            }
        },
        series: [{
            name: 'Thank you',
            data: [5, 3, 4, 7, 2]
        }, {
            name: 'Bright Idea',
            data: [2, 2, 3, 2, 1]
        }, {
            name: 'Make it Happen',
            data: [3, 4, 4, 2, 5]
        },{
            name: 'Colaboration',
            data: [2, 2, 3, 2, 1]
        },
        {
            name: 'Customer Service Excelence',
            data: [2, 2, 3, 2, 1]
        },
        {
            name: 'Inspiring',
            data: [2, 2, 3, 2, 1]
        },

        ]
    });


    $('#container1').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: [
                'Human Resources',
                'IT',
                'Finance',
                'Sales',
                'Marketing',
                'Customer Experience',
                'Facilities',
                
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        credits: {
      enabled: false
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Awards Received',
            data: [100, 75, 50, 75, 50, 75, 100]

        }, {
            name: 'Awards Given',
            data: [83, 68, 40, 65, 40, 84, 88]

        }]
    });

   var chart=$('#container2').highcharts({
        chart: {
            type: 'pie',
            plotBackgroundColor: null,
            plotBorderWidth: 0,
            plotShadow: false,
             marginTop: 10,
            marginBottom: 100,
            marginLeft: 50,
            marginRight: 110
        },
        title: {
            text: '',
            align: 'center',
            verticalAlign: 'middle',
            y: 50
        },
        credits: {
      enabled: false
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                dataLabels: {
                    enabled: true,                    
                    style: {
                        fontWeight: 'bold',
                        color: 'black',
                    }
                },
                size: '100%',
                startAngle: -180,
                endAngle: 180,
                center: ['50%', '75%']
            }
        },
        series: [{
            type: 'pie',
            size: '100%',
            name: 'Budget Usage',
            marginTop:0,
            marginLeft:-100,
            innerSize: '50%',
            data: [
                ['IT',   15.0],
                ['HR',       26.8],
                ['Finance', 22.8],
                ['Facilities',    15.5],
                ['Customer Exp',     10.2],
                ['Marketing',   5.7],
                ['Sales',   5.7]
            ]
        }]
    });


 var barChart = $('#container3').highcharts({
        chart: {
            type: 'bar'
        },
        title: {
            text: ''    
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: ['Share your favorite Healthy recipe', 'Random Act of Kindness Challenge', 'Go, Green| Nuture a plant at your desk', 'Photography in and around workplace'],
            title: {
                text: null
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: '',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        tooltip: {
            valueSuffix: ''
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        credits: {
      enabled: false
        },
        /*legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'bottom',
            x: 0,
            y: 0,
            floating: true,
            borderWidth: 1,
            //backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
            shadow: true
        },*/
        credits: {
            enabled: false
        },
        series: [{
            name: 'Posts',
            data: [700, 650, 1000, 850]
        }, {
            name: 'Votes',
            data: [880, 800, 850, 980]
        },]
    });

   

    $('#container4').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: [
                '19/10/2015 - 25/10/2015',
                '26/10/2015 - 01/11/2015',
                '02/11/2015 - 08/11/2015',
                '09/11/2015 - 15/11/2015',
                '16/11/2015 - 22/11/2015',                
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        credits: {
      enabled: false
  },
        series: [{
            name: 'Daily Happenings',
            data: [200, 271, 240, 150, 300]

        }]
    });

 
});